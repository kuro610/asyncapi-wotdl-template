import { File, Text } from "@asyncapi/generator-react-sdk";

export default function ({ params }) {
  return (
    <File name="hub.py">
      <Text>import importlib</Text>
      <Text>import logging</Text>
      <Text newLines={2}>from importlib.util import find_spec</Text>
      <Text newLines={3}>
        from gmqtt.mqtt.constants import PubRecReasonCode
      </Text>
      <Text newLines={3}>mainLogger = logging.getLogger("MAIN")</Text>
      <Text>IMPLEMENTATION_PATH = "{params.moduleName}.models"</Text>
      <Text newLines={2}>mainLogger.info("HUB imported")</Text>
      <Text>PERSISTENCE = {"{}"}</Text>
      <Text newLines={3}>INIT_FUNCTION_NAME = "init"</Text>
      <Text>
        def invoke_implementation(action: str, device: str, params: dict) {"->"}
        tuple[int, dict]:
      </Text>
      <Text indent={4} newLines={2}>
        import_path = IMPLEMENTATION_PATH + "." + device
      </Text>
      <Text indent={4}># search device implementation module</Text>
      <Text indent={4}>implementation_spec = find_spec(import_path)</Text>
      <Text indent={4} newLines={2}>
        found = implementation_spec is not None
      </Text>
      <Text indent={4}>if found:</Text>
      <Text indent={8} newLines={2}>
        implementation = importlib.import_module(import_path)
      </Text>
      <Text indent={8}># initialize device</Text>
      <Text indent={8}>if hasattr(implementation, INIT_FUNCTION_NAME):</Text>
      <Text indent={12}>
        plugin_init_function = getattr(implementation, INIT_FUNCTION_NAME)
      </Text>
      <Text indent={12} newLines={2}>
        plugin_init_function()
      </Text>
      <Text indent={8}># search action</Text>
      <Text indent={8}>if not hasattr(implementation, action):</Text>
      <Text indent={12}>
        error = f"Implementation required for {"{action}"} of device{" "}
        {"{device}"}"
      </Text>
      <Text indent={12}>mainLogger.error(error)</Text>
      <Text indent={12} newLines={2}>
        return PubRecReasonCode.IMPLEMENTATION_SPECIFIC_ERROR,{" "}
        {`{"error": error}`}
      </Text>
      <Text indent={8} newLines={2}>
        method = getattr(implementation, action)
      </Text>
      <Text indent={8}>res = method(**params)</Text>
      <Text indent={8}>if not res:</Text>
      <Text indent={12} newLines={2}>
        res = {"{}"}
      </Text>
      <Text indent={8} newLines={2}>
        return PubRecReasonCode.SUCCESS, res
      </Text>
      <Text indent={4}>else:</Text>
      <Text indent={8}>
        error = f"Implementation required for device {"{device}"}"
      </Text>
      <Text indent={8}>mainLogger.error(error)</Text>
      <Text indent={8}>
        return PubRecReasonCode.IMPLEMENTATION_SPECIFIC_ERROR,{" "}
        {`{"error": error}`}
      </Text>
    </File>
  );
}
