import { File, Text } from "@asyncapi/generator-react-sdk";

export default function ({ asyncapi }) {
  return (
    <File name="README.md">
      <Text newLines={2}># {asyncapi.info().title()}</Text>
      <Text newLines={2}>## Version {asyncapi.info().version()}</Text>
      <Text>{asyncapi.info().description()}</Text>
    </File>
  );
}
