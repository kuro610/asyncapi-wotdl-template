import logging


consoleHandler = logging.StreamHandler()
logging.basicConfig(
    format="%(asctime)s\t%(levelname)s\t%(name)s\t%(message)s",
    handlers=[consoleHandler],
)
mainLogger = logging.getLogger("MAIN")
mainLogger.setLevel(logging.DEBUG)
mainLogger.propagate = False
mainLogger.addHandler(consoleHandler)
