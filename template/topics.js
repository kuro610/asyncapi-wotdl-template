import { File, Text } from "@asyncapi/generator-react-sdk";

import { Subscription } from "../components/Subscription";
import { Publish } from "../components/Publish";

export default function ({ asyncapi, params }) {
  return (
    <File name="topics.py">
      <Text>from .channel_parser import pub_topic, sub_topic</Text>
      <Text newLines={3}>from .custom_types import PubTopic, SubTopic</Text>
      <Text>SUBSCRIPTION_TOPICS: list[SubTopic] = [</Text>
      {Object.entries(asyncapi.channels())
        .filter(elem => elem[1].subscribe())
        .map((elem, idx) => (
          <Subscription key={idx} name={elem[0]} channel={elem[1]} />
        ))}
      <Text newLines={3}>]</Text>
      <Text>PUBLISH_TOPICS: list[PubTopic] = [</Text>
      {Object.entries(asyncapi.channels())
        .filter(elem => elem[1].publish())
        .map((elem, idx) => (
          <Publish key={idx} name={elem[0]} channel={elem[1]} />
        ))}
      <Text>]</Text>
    </File>
  );
}
