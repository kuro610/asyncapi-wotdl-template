from typing import Any, Literal

from .custom_types import PubTopic, SubTopic


def sub_topic(
    path: str,
    device: str,
    action: str,
    params: list[tuple[str, Literal["str", "int"]]] = [],
) -> SubTopic:
    path_params = []
    topic = {"topic": path, "catch_all": False, "device": device, "action": action}

    path = path.split("/")

    param_idx = 1
    for part in path:
        if part == "+":
            try:
                path_params.append(params.pop(0))
            except IndexError:
                path_params.append((f"param_{param_idx}", "str"))
                param_idx += 1

    if path[-1] == "#":
        try:
            path_params.append(params.pop(0))
        except IndexError:
            path_params.append((f"param_{param_idx}", "str"))

        topic["catch_all"] = True
        path.pop()

    topic["split_topic"] = tuple(path)
    topic["params"] = tuple(path_params)

    return topic


def pub_topic(
    topic: str, device: str, action: str, params: list[tuple[str, Any]] = []
) -> PubTopic:
    return {
        "topic": topic,
        "device": device,
        "action": action,
        "params": dict(params),
        "interval": 4000,
    }
