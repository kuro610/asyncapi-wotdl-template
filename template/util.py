import asyncio
import json
import logging

from gmqtt import Client as MQTTClient
from gmqtt.mqtt.constants import PubRecReasonCode

from .hub import invoke_implementation
from .topics import SUBSCRIPTION_TOPICS
from .custom_types import MessageProps


mainLogger = logging.getLogger("MAIN")


STOP: asyncio.Event | None = None


def ask_exit():
    try:
        STOP.set()
    except:
        pass


def on_connect(client: MQTTClient, flags, rc, properties):
    mainLogger.info(f"Connected to broker {client._host}:{client._port}")


def on_disconnect(client: MQTTClient, packet, exc=None):
    mainLogger.info(f"Disconnected from broker {client._host}:{client._port}")


async def dispatch_message(
    client: MQTTClient, topic: str, payload: bytes, qos: int, properties: MessageProps
) -> int:
    if properties["dup"]:
        return PubRecReasonCode.SUCCESS

    params = {}
    action: str | None = None
    device: str | None = None
    split_topic = topic.split("/")
    for sub_topic in SUBSCRIPTION_TOPICS:
        if not sub_topic["catch_all"] and len(split_topic) != len(
            sub_topic["split_topic"]
        ):
            continue

        match = True
        param_idx = 0
        for i in range(len(sub_topic["split_topic"])):
            if split_topic[i] == sub_topic["split_topic"][i]:
                continue

            if sub_topic["split_topic"][i] == "+":
                try:
                    tmp = split_topic[i]
                    if sub_topic["params"][param_idx][1] == "int":
                        tmp = int(tmp)

                    params[sub_topic["params"][param_idx][0]] = tmp
                    continue
                except:
                    pass

            match = False
            break

        if match:
            mainLogger.debug(f"Matched topic: {sub_topic['topic']}")
            action = sub_topic["action"]
            device = sub_topic["device"]
            break

    if not action:
        mainLogger.debug("No topic matched")
        return PubRecReasonCode.NO_MATCHING_SUBSCRIBERS

    try:
        params["body"] = json.loads(payload)
    except:
        params["body"] = payload.decode()
        mainLogger.debug("No JSON payload")
    mainLogger.debug(f"Payload: {params}")

    try:
        res = invoke_implementation(action, device, params)
    except Exception as e:
        mainLogger.error(e)
        return PubRecReasonCode.UNSPECIFIED_ERROR

    return res[0]


async def publish(
    client: MQTTClient,
    topic: str,
    action: str,
    device: str,
    interval: int,
    params: dict,
):
    mainLogger.info(f"Publish every {interval}ms to {topic}")
    try:
        while True:
            try:
                payload = invoke_implementation(action, device, params)
            except Exception as e:
                mainLogger.error(e)
                payload = [None, {"error": "An unexpected error occured."}]
            client.publish(topic, json.dumps(payload[1]), 1, True)
            await asyncio.sleep(interval / 1000)
    except asyncio.CancelledError:
        mainLogger.info(f"End publishing to {topic}")
