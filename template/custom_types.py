from typing import Literal, TypedDict


class SubTopic(TypedDict):
    topic: str
    split_topic: tuple[str]
    catch_all: bool
    action: str
    device: str
    params: tuple[tuple[str, Literal["str", "int"]]]
    pass


class PubTopic(TypedDict):
    topic: str
    action: str
    device: str
    params: dict
    pass


class MessageProps(TypedDict):
    dup: int
    retain: int
    pass
