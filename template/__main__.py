import asyncio
import configparser
import logging
import signal

from gmqtt import Client as MQTTClient
import uvloop

from .topics import PUBLISH_TOPICS, SUBSCRIPTION_TOPICS
from .util import *


mainLogger = logging.getLogger("MAIN")


# Config has the connection properties.
def getConfig():
    configParser = configparser.ConfigParser()
    configParser.read("config.ini")
    config = configParser["DEFAULT"]
    return config


async def main():
    STOP = asyncio.Event()
    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGINT, ask_exit)
    loop.add_signal_handler(signal.SIGTERM, ask_exit)

    config = getConfig()

    client = MQTTClient(config.get("client_id", "wotdl-api"))

    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.on_message = dispatch_message

    if config.get("username", None):
        client.set_auth_credentials(
            config.get("username"), config.get("password", None)
        )

    await client.connect(
        config.get("broker_host", "localhost"), config.get("broker_port", 1883)
    )

    for topic in SUBSCRIPTION_TOPICS:
        mainLogger.info(f"Subscribe to {topic['topic']}")
        client.subscribe(topic["topic"], 2)

    pub_tasks: list[asyncio.Task] = []
    for pub in PUBLISH_TOPICS:
        pub_tasks.append(asyncio.create_task(publish(client, **pub)))

    try:
        await STOP.wait()
    except:
        pass

    for task in pub_tasks:
        task.cancel()
    await asyncio.sleep(0.5)
    await client.disconnect()


if __name__ == "__main__":
    uvloop.install()
    asyncio.run(main())
