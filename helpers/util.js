const CHANNEL_PARAM_REGEX = /{(?<param>.+?)}/g;
const TYPES = { string: "str", integer: "int", number: "float" };

function logAllProperties(obj) {
  if (obj == null) return; // recursive approach
  console.log(Object.getOwnPropertyNames(obj));
  logAllProperties(Object.getPrototypeOf(obj));
}

const cartesian = (...a) =>
  a.reduce((a, b) => a.flatMap(d => b.map(e => [d, e].flat())));

export { CHANNEL_PARAM_REGEX, TYPES, cartesian, logAllProperties };
