# WoTDL API template

- [Overview](#overview)
- [Technical requirements](#technical-requirements)
- [Specification requirements](#specification-requirements)
- [Supported protocols](#supported-protocols)
- [How to use the template](#how-to-use-the-template)
  * [CLI](#cli)
- [Template configuration](#template-configuration)
- [Development](#development)


## Overview

<!--  
The overview should explain in just a few sentences the template's purpose and its most essential features.
-->

This template generates python code to execute the specified API. The generated code is a MQTT client based on the python lib gmqtt. To use the you need to provide the required device implementations.

## Technical requirements

<!--  
Specify what version of the Generator is your template compatible with. This information should match the information provided in the template configuration under the `generator` property.
-->

- 1.3.0 =< [Generator](https://github.com/asyncapi/generator/) < 2.0.0,
- Generator specific [requirements](https://github.com/asyncapi/generator/#requirements)

## Specification requirements

<!--  
The template might need some AsyncAPI properties that normally are optional. For example code generator might require some specific binding information for a given protocol. Even though you can provide defaults or fallbacks, you should describe in the readme what is the most optimal set of properties that the user should provide in the AsyncAPI file.
-->

The table contains information on parts of the specification required by this template to generate the proper output.

Property name | Reason | Fallback | Default
---|---|---|---
`channels.PATH.[publish\|subscribe].operationId` | This template uses the operation id the decide which device function needs to be called. Therfor the required format is `DEVICE_NAME###ACTION_NAME`. | - | -
`channels.PATH.parameters.PARAMETER_NAME.schema` | In the parameter schema the `type` attribute must be `integer` or `string`. Also the `enum` atribute is required and must contain all possible parameter values (Important for publish opertations). | - | -

## Supported protocols

<!--  
Specify what protocols is your code generator supporting. This information should match the information provided in the template configuration under the `supportedProtocols` property. Don't put this section in your readme if your template doesn't generate code.
-->

- MQTT v5 and below

## How to use the template

<!--  
Make sure it is easy to try out the template and check what it generates. Instructions for CLI and Docker should be easy to use; just copy/paste to the terminal. In other words, you should always make sure to have ready to use docker-compose set up so the user can quickly check how generated code behaves.
-->

This template must be used with the AsyncAPI Generator. You can find all available options [here](https://github.com/asyncapi/generator/).

### CLI

```bash
npm install -g @asyncapi/generator
ag PATH/TO/ASYNCAPI_SPEC PATH/TO/THIS/GENERATOR/TEMPLATE -o output
cd output
python3 -m wot_async_api
```


## Template configuration

<!--  
This information should match the information provided in the template configuration under the `parameters` property.
-->

You can configure this template by passing different parameters in the Generator CLI: `-p PARAM1_NAME=PARAM1_VALUE -p PARAM2_NAME=PARAM2_VALUE`

Name | Description | Required | Default | Allowed Values | Example
---|---|---|---|---|---
asyncapiFileDir | Custom location of the AsyncAPI file that you provided as an input in a generation. | No | The root of the output directory. | Path to the custom directory relative to the root of output directory provided as a string. | `/custom/dir`
moduleName | Name of the generated python module. | No | `wot_async_api` | string with allowed python module name | `raspi_api`


## Development

<!--  
This section will look the same everywhere, just make sure it references your template.
-->

The most straightforward command to use this template is:
```bash
ag https://raw.githubusercontent.com/asyncapi/generator/v1.0.1/test/docs/dummy.yml PATH/TO/THIS/GENERATOR/TEMPLATE -o output
```

For local development, you need different variations of this command. First of all, you need to know about three important CLI flags:
- `--debug` enables the debug mode in Nunjucks engine what makes filters debugging simpler. 
- `--watch-template` enables a watcher of changes that you make in the template. It regenerates your template whenever it detects a change.
- `--install` enforces reinstallation of the template.

There are two ways you can work on template development:
- Use global Generator and template from your local sources:
  ```bash
  # assumption is that you run this command from the root of your template
  ag https://raw.githubusercontent.com/asyncapi/generator/v1.0.1/test/docs/dummy.yml ./ -o output
  ```
- Use Generator from sources and template also from local sources. This approach enables more debugging options with awesome `console.log` in the Generator sources or even the Parser located in `node_modules` of the Generator:
  ```bash
  # assumption is that you run this command from the root of your template
  # assumption is that generator sources are cloned on the same level as the template
  ../generator/cli.js https://raw.githubusercontent.com/asyncapi/generator/v1.0.1/test/docs/dummy.yml ./ -o output
  ```
