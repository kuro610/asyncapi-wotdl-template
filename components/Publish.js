import { Text } from "@asyncapi/generator-react-sdk";

import { CHANNEL_PARAM_REGEX, TYPES, cartesian } from "../helpers/util";

function Publish({ name, channel }) {
  const [device, action] = channel.publish().id().split("###");
  let params = [];
  for (const match of name.matchAll(CHANNEL_PARAM_REGEX)) {
    params.push([
      match.groups.param,
      channel.parameters()[match.groups.param].schema().enum(),
    ]);
  }
  let topics = [name];
  if (params.length) {
    topics = getCombinations(name, params);
  }
  return (
    <>
      {topics.map((elem, idx) => (
        <Text key={idx} indent={4}>
          pub_topic("{elem}", "{device}", "{action}"),
        </Text>
      ))}
    </>
  );
}

function getCombinations(name, params) {
  const keys = params.map((elem, idx) => [elem[0], idx]);
  const combos = cartesian(...params.map(elem => elem[1]));

  let pubTopics = [];
  for (let combo of combos) {
    let topic = name;
    for (let key of keys) {
      topic = topic.replace(`{${key[0]}}`, combo[key[1]]);
    }
    pubTopics.push(topic);
  }

  return pubTopics;
}

export { Publish };
