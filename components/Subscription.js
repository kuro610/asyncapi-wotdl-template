import { Text } from "@asyncapi/generator-react-sdk";

import { CHANNEL_PARAM_REGEX, TYPES } from "../helpers/util";

function Subscription({ name, channel }) {
  const [device, action] = channel.subscribe().id().split("###");
  let params = [];
  for (const match of name.matchAll(CHANNEL_PARAM_REGEX)) {
    params.push(
      `("${match.groups.param}", "${
        TYPES[channel.parameters()[match.groups.param]._json.schema.type]
      }")`
    );
  }

  params = params.join(", ");

  return (
    <Text indent={4}>
      sub_topic("{name.replace(CHANNEL_PARAM_REGEX, "+")}", "{device}", "
      {action}"{params ? `, [${params}]` : ""}),
    </Text>
  );
}

export { Subscription };
